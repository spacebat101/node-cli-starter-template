import Thing from './Thing.js';

describe('Thing', () => {
    describe('helloWorld()', () => {
        test('should say Hello', () => {
            expect(Thing.helloWorld()).toEqual("Hello World");
        });
    })
});
