# README

# To Create a New Repo from this Template

1. `npx degit [this repo url]`
2. `git init`

## To Install

1. Open a command line in your product directory
2. `npm install`

## To Run

1. Open a command line
2. Run the app

## To Test

### One-off

`npm run test`

### Watching for changes

`npm run test:watch`

### Coverage

`npm run test:coverage`

## As a standalone app

### To set up

1. Install `pkg`
   1. `npm install -g pkg`
2. Build the node app
3. Create the standalone (mac) app, from the node app
   1. `pkg ./dist/app.js --output ./bin/thing`

### To run

1. Run the app
   1. `./bin/thing`

Enjoy!
